#!/usr/bin/env bash
set -ex

mvn clean install

$GRAALVM_HOME/bin/native-image -cp ./target/kotlin-cli-example-1.0-SNAPSHOT-jar-with-dependencies.jar -H:Name=kotlincli -H:Class=org.example.MyApp -H:+ReportUnsupportedElementsAtRuntime --allow-incomplete-classpath
