package org.example

fun main() {
    println("Hello kotlin cli 2")
}

class CliMain {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            println("Hello kotlin cli")
        }
    }
}