package org.example

import org.example.subcommands.SubCmd
import org.example.subcommands.TopCmd
import picocli.CommandLine
import java.util.concurrent.Callable

//fun main() {
//    println("Hello kotlin cli 3")
//}

//fun main(args: Array<String>) = System.exit(CommandLine(MyApp()).execute(*args))

@CommandLine.Command(name = "MyApp", version = ["Kotlin picocli demo v4.0"],
    mixinStandardHelpOptions = true,
    description = ["@|bold Kotlin|@ @|underline picocli|@ example"],
    subcommands = [picocli.CommandLine.HelpCommand::class, SubCmd::class, TopCmd::class])
class MyApp : Callable<Int> {

    @CommandLine.Option(names = ["-c", "--count"], paramLabel = "COUNT",
        description = ["the count"])
    private var count: Int = 0

    override fun call(): Int {
        for (i in 0 until count) {
            println("hello world $i...")
        }
        return 123
    }
    companion object {
        @JvmStatic fun main(args: Array<String>) {
            CommandLine(MyApp()).execute(*args)
        }
    }
}
