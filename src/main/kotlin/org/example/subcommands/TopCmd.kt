package org.example.subcommands

import picocli.CommandLine

@CommandLine.Command(name = "top", version = ["Kotlin picocli demo v3.8.1"],
    mixinStandardHelpOptions = true,
    description = ["@|bold Kotlin|@ @|underline picocli|@ example"],
    subcommands = [SubCmd::class])
class TopCmd : Runnable {
    @CommandLine.Spec
    var spec: CommandLine.Model.CommandSpec? = null

    override fun run() {
        throw CommandLine.ParameterException(spec?.commandLine(), "Specify a subcommand")
    }
    companion object {
        @JvmStatic fun main(args: Array<String>) {
            CommandLine(TopCmd()).execute(*args)
        }
    }
}