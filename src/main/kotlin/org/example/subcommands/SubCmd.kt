package org.example.subcommands

import picocli.CommandLine

@CommandLine.Command(name = "sub", mixinStandardHelpOptions = true,
    description = ["I'm a subcommand. Prints help if count=7."])
class SubCmd : Runnable {

    @CommandLine.Option(names = ["-c", "--count"], paramLabel = "COUNT",
        description = ["the count"])
    private var count: Int = 0

    @CommandLine.Spec
    var spec: CommandLine.Model.CommandSpec? = null

    override fun run() {
        println("I'm a subcommand")
        for (i in 0 until count) {
            println("hello world $i...")
        }
        if (count == 7) {
            spec?.commandLine()?.usage(System.out)
        }
    }
}