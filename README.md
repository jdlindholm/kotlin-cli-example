## Kotlin cli with Picocli for Graalvm

Inspiration:

* https://www.graalvm.org/docs/examples/java-kotlin-aot/
* https://picocli.info/#_graalvm_native_image
* https://github.com/remkop/picocli/tree/master/picocli-codegen
* https://github.com/remkop/picocli/tree/master/picocli-examples/src/main/kotlin/picocli/examples/kotlin
* https://medium.com/graalvm/simplifying-native-image-generation-with-maven-plugin-and-embeddable-configuration-d5b283b92f57

Remember to add annotation processor for kapt to generate GraalVM configuration with picocli-codegen. See pom.
 